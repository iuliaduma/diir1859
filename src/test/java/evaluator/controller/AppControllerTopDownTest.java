package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepositoryTest;
import org.junit.Test;

import static org.junit.Assert.*;

public class AppControllerTopDownTest {

    IntrebariRepositoryTest repositoryTest = new IntrebariRepositoryTest();
    AppControllerWBTTest controllerWBTTest = new AppControllerWBTTest();
    AppControllerStatisticaTest statisticaTest = new AppControllerStatisticaTest();
    AppController controller = new AppController();

    @Test
    public void testUnitA(){
        try {
            repositoryTest.addIntrebare();
        } catch (DuplicateIntrebareException e) {
            assertTrue(e.getMessage().length()>0);
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }

    @Test
    public void testIntB(){
        controller.loadIntrebariFromFile("intrebari.txt");
        String msg="";
        try {
            Intrebare intrebare;
            intrebare = controller.addNewIntrebare(new Intrebare("Test?", "1)Da", "2)Nu", "3)Poate", "3", "Test"));
            System.out.println(intrebare);
            System.out.println();
            assertFalse(intrebare==null);
        } catch (DuplicateIntrebareException e) {
            assertTrue(e.getMessage().length()>0);
            msg+=e.getMessage();
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
            msg+=e.getMessage();
        }
        assertTrue(msg=="");
        try {
            evaluator.model.Test test;
            test = controller.createNewTest();
            System.out.println(test);
            assertFalse(test==null);
        } catch (NotAbleToCreateTestException e) {
            assertTrue(e.getMessage().length()>0);
            msg+=e.getMessage();
        }
        assertTrue(msg=="");
    }

    @Test
    public void testIntC(){
        controller.loadIntrebariFromFile("intrebari.txt");
        String msg="";
        try {
            Intrebare intrebare;
            intrebare = controller.addNewIntrebare(new Intrebare("Test1?", "1)Da", "2)Nu", "3)Poate", "3", "Test"));
            System.out.println(intrebare);
            System.out.println();
            assertFalse(intrebare==null);
        } catch (DuplicateIntrebareException e) {
            assertTrue(e.getMessage().length()>0);
            msg+=e.getMessage();
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
            msg+=e.getMessage();
        }
        assertTrue(msg=="");
        try {
            evaluator.model.Test test;
            test = controller.createNewTest();
            System.out.println(test);
            assertFalse(test==null);
        } catch (NotAbleToCreateTestException e) {
            assertTrue(e.getMessage().length()>0);
            msg+=e.getMessage();
        }
        assertTrue(msg=="");
        try {
            Statistica statistica;
            statistica = controller.getStatistica();
            System.out.println(statistica);
            assertFalse(statistica==null);
        } catch (NotAbleToCreateStatisticsException e) {
            assertTrue(e.getMessage().length()>0);
            msg+=e.getMessage();
        }
        assertTrue(msg=="");
    }
}