package evaluator.controller;

import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Statistica;
import org.junit.Test;

import static org.junit.Assert.*;

public class AppControllerStatisticaTest {

    AppController controller = new AppController();

    @Test
    public void getStatistica() {
        controller.loadIntrebariFromFile("intrebari.txt");
        try {
            Statistica statistica;
            statistica = controller.getStatistica();
            System.out.println(statistica);
            System.out.println();
            assertFalse(statistica==null);
        } catch (NotAbleToCreateStatisticsException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }

    @Test
    public void getStatisticaVid() {
        controller.loadIntrebariFromFile("intrebariVid.txt");
        try {
            Statistica statistica;
            statistica = controller.getStatistica();
            System.out.println(statistica);
            assertFalse(statistica==null);
        } catch (NotAbleToCreateStatisticsException e) {
            System.out.println(e.getMessage());
            System.out.println();
            assertTrue(e.getMessage().length()>0);
        }
    }
}