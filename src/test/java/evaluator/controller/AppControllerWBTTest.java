package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AppControllerWBTTest {

    IntrebariRepository repository = new IntrebariRepository();
    AppController controller = new AppController();

    @Test
    public void createNewTest1() throws InputValidationFailedException, DuplicateIntrebareException {
        controller.addNewIntrebare(new Intrebare("Test1?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test2?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test3?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test4?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        System.out.println(repository.getIntrebari());
        try {
            evaluator.model.Test test = controller.createNewTest();
            assertFalse(test==null);
        } catch (NotAbleToCreateTestException e) {
            assertTrue( e.getMessage().length() > 0);
        }

    }

    @Test
    public void createNewTest2() throws InputValidationFailedException, DuplicateIntrebareException {
        controller.addNewIntrebare(new Intrebare("Test1?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test2?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test3?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test4?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test5?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        System.out.println(repository.getIntrebari());
        try {
            evaluator.model.Test test = controller.createNewTest();
            assertFalse(test== null);
        } catch (NotAbleToCreateTestException e) {
            assertTrue( e.getMessage().length() > 0);
        }
    }

    @Test
    public void createNewTest3() throws InputValidationFailedException, DuplicateIntrebareException {
        controller.addNewIntrebare(new Intrebare("Test1?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test2?", "1)Da", "2)Nu", "3)Poate", "3", "Test"));
        controller.addNewIntrebare(new Intrebare("Test3?", "1)Da", "2)Nu", "3)Poate", "3", "B"));
        controller.addNewIntrebare(new Intrebare("Test4?", "1)Da", "2)Nu", "3)Poate", "3", "C"));
        controller.addNewIntrebare(new Intrebare("Test5?", "1)Da", "2)Nu", "3)Poate", "3", "D"));
        controller.addNewIntrebare(new Intrebare("Test6?", "1)Da", "2)Nu", "3)Poate", "3", "E"));
        //System.out.println(repository.getIntrebari());
        try {
            evaluator.model.Test test = controller.createNewTest();
            System.out.println(test);
            assertFalse(test==null);
        } catch (NotAbleToCreateTestException e) {
            assertFalse(e.getMessage().length()>0);
        }
    }
    @Test
    public void createNewTest4() throws InputValidationFailedException, DuplicateIntrebareException {
        controller.addNewIntrebare(new Intrebare("Test1?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test2?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test3?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test4?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test5?", "1)Da", "2)Nu", "3)Poate", "3", "Test"));
        controller.addNewIntrebare(new Intrebare("Test6?", "1)Da", "2)Nu", "3)Poate", "3", "B"));
        controller.addNewIntrebare(new Intrebare("Test7?", "1)Da", "2)Nu", "3)Poate", "3", "C"));
        controller.addNewIntrebare(new Intrebare("Test8?", "1)Da", "2)Nu", "3)Poate", "3", "D"));
        controller.addNewIntrebare(new Intrebare("Test9?", "1)Da", "2)Nu", "3)Poate", "3", "E"));
        //System.out.println(repository.getIntrebari());
        try {
            evaluator.model.Test test=controller.createNewTest();
            System.out.println(test);
            assertFalse(test==null);
        } catch (NotAbleToCreateTestException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }

    @Test
    public void createNewTest5() throws InputValidationFailedException, DuplicateIntrebareException {
        controller.addNewIntrebare(new Intrebare("Test1?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test2?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test3?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test4?", "1)Da", "2)Nu", "3)Poate", "3", "A"));
        controller.addNewIntrebare(new Intrebare("Test5?", "1)Da", "2)Nu", "3)Poate", "3", "Test"));
        controller.addNewIntrebare(new Intrebare("Test6?", "1)Da", "2)Nu", "3)Poate", "3", "B"));
        controller.addNewIntrebare(new Intrebare("Test7?", "1)Da", "2)Nu", "3)Poate", "3", "C"));
        controller.addNewIntrebare(new Intrebare("Test8?", "1)Da", "2)Nu", "3)Poate", "3", "D"));
        controller.addNewIntrebare(new Intrebare("Test9?", "1)Da", "2)Nu", "3)Poate", "3", "E"));
        try {
            evaluator.model.Test test = controller.createNewTest();
            assertFalse(test==null);
        } catch (NotAbleToCreateTestException e) {
            assertFalse( e.getMessage().length() == 0);
        }
    }



}