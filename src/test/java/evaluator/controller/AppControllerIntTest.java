package evaluator.controller;

import org.junit.Test;

import static org.junit.Assert.*;

public class AppControllerIntTest {

    AppControllerBigBangTest controllerBigBangTest = new AppControllerBigBangTest();
    AppControllerTopDownTest controllerTopDownTest = new AppControllerTopDownTest();

    @Test
    public void testBigBang() {
        controllerBigBangTest.testUnitA();
        controllerBigBangTest.testUnitB();
        controllerBigBangTest.testUnitC();
        controllerBigBangTest.testBigBang();
    }

    @Test
    public void testTopDown(){
        controllerTopDownTest.testUnitA();
        controllerTopDownTest.testIntB();
        controllerTopDownTest.testIntC();
    }
}