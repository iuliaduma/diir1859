package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IntrebariRepositoryTest {

    IntrebariRepositoryTestECPValid repositoryTestECPValid = new IntrebariRepositoryTestECPValid();
    IntrebariRepositoryTestECPInvalid repositoryTestECPInvalid = new IntrebariRepositoryTestECPInvalid();
    IntrebariRepositoryTestBVAValid repositoryTestBVAValid = new IntrebariRepositoryTestBVAValid();
    IntrebariRepositoryTestBVAInvalid repositoryTestBVAInvalid = new IntrebariRepositoryTestBVAInvalid();

    @Test
    public void addIntrebare() throws DuplicateIntrebareException, InputValidationFailedException {
        repositoryTestECPValid.addIntrebare();
        repositoryTestECPInvalid.addIntrebareCorecta1();
        repositoryTestECPInvalid.addIntrebareDuplicate();
        repositoryTestECPInvalid.addIntrebareLiteraMare();
        repositoryTestECPInvalid.addIntrebareSfarsitEnunt();
        repositoryTestECPInvalid.addIntrebareVariantaCorecta();
        repositoryTestECPInvalid.addIntrebareVariantadeRaspuns();
        repositoryTestECPInvalid.addIntrebareVariantadeRaspuns1();
        repositoryTestECPInvalid.addIntrebareVid();
        repositoryTestBVAValid.addIntrebare();
        repositoryTestBVAValid.addIntrebareDomeniuMax();
        repositoryTestBVAValid.addIntrebareEnuntMax();
        repositoryTestBVAValid.addIntrebareVarianta1Max();
        repositoryTestBVAValid.addIntrebareVarianta2Max();
        repositoryTestBVAValid.addIntrebareVarianta3MAx();
        repositoryTestBVAValid.addIntrebareVariantaCorecta();
        repositoryTestBVAInvalid.addIntrebareDomeniuLung();
        repositoryTestBVAInvalid.addIntrebareEnuntLung();
        repositoryTestBVAInvalid.addIntrebareEnuntScurt();
        repositoryTestBVAInvalid.addIntrebareVarianta1Lung();
        repositoryTestBVAInvalid.addIntrebareVarianta1Scurt();
        repositoryTestBVAInvalid.addIntrebareVarianta2Lung();
        repositoryTestBVAInvalid.addIntrebareVarianta2Scurt();
        repositoryTestBVAInvalid.addIntrebareVarianta3Lung();
        repositoryTestBVAInvalid.addIntrebareVarianta3Scurt();
        repositoryTestBVAInvalid.addIntrebareVariantaCorecta();
    }
}