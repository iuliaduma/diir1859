package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IntrebariRepositoryTestBVAValid {

    IntrebariRepository repository = new IntrebariRepository();


    @Test
    public void addIntrebare() throws InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Test1?", "1)Da", "2)Nu", "3)Poate", "3", "Test");
        try {
            repository.addIntrebare(intrebare);
        } catch (DuplicateIntrebareException e) {
            assertFalse(e.getMessage().length() > 0);
        }
    }
    @Test
    public void addIntrebareEnuntMax() throws InputValidationFailedException {
        Intrebare intrebare1 = new Intrebare("TestasdfghjklqwertyuiopzxcvbnmasdfghjklqwertyuiopTestasdfghjklqwertyuiopzxcvbnmasdfghjklqwertyuiopa?", "1)Da", "2)Nu", "3)Poate", "3", "Test");
        try {
            repository.addIntrebare(intrebare1);
        } catch (DuplicateIntrebareException e) {
            assertFalse(e.getMessage().length() > 0);
        }
    }

    @Test
    public void addIntrebareVarianta1Max() throws InputValidationFailedException {
        Intrebare intrebare2 = new Intrebare("Test2?", "1)Daasdfghjklqwertyuiopzxcvbnmqwertyuiopzxcvbnmasd", "2)Nu", "3)Poate", "3", "Test");
        try {
            repository.addIntrebare(intrebare2);
        } catch (DuplicateIntrebareException e) {
            assertFalse(e.getMessage().length()>0);
        }
    }

    @Test
    public void addIntrebareVarianta2Max()throws InputValidationFailedException {
        Intrebare intrebare3 = new Intrebare("Test3?", "1)Da", "2)Nuasdfghjklqwertyuiopzxcvbnmqwertyuiopzxcvbnmasd", "3)Poate", "3", "Test");
        try {
            repository.addIntrebare(intrebare3);
        } catch (DuplicateIntrebareException e) {
            assertFalse(e.getMessage().length() > 0);
        }
    }
    @Test
    public void addIntrebareVarianta3MAx() throws InputValidationFailedException {
        Intrebare intrebare4 = new Intrebare("Test4?", "1)Da", "2)Nu", "3)Poateasdfghjklqwertyuiopzxcvbnmqwertyuiopzxcvbnm", "3", "Test");
        try {
            repository.addIntrebare(intrebare4);
        } catch (DuplicateIntrebareException e) {
            assertFalse(e.getMessage().length()>0);
        }
    }
    @Test
    public void addIntrebareVariantaCorecta() throws InputValidationFailedException {
        Intrebare intrebare5 = new Intrebare("Test5?", "1)Da", "2)Nu", "3)Poate", "3", "Test");
        try {
            repository.addIntrebare(intrebare5);
        } catch (DuplicateIntrebareException e) {
            assertFalse(e.getMessage().length()>0);
        }
    }
    @Test
    public void addIntrebareDomeniuMax() throws InputValidationFailedException{
        Intrebare intrebare6 = new Intrebare("Test6?", "1)Da", "2)Nu", "3)Poate", "3", "Testwertyuiopzxcvbnmqwertyuiop");
        try {
            repository.addIntrebare(intrebare6);
        } catch (DuplicateIntrebareException e) {
            assertFalse(e.getMessage().length()>0);
        }
    }
}