package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IntrebariRepositoryTestECPValid {

    IntrebariRepository repository = new IntrebariRepository();

    @Test
    public void addIntrebare() throws InputValidationFailedException, DuplicateIntrebareException {
        try {
            Intrebare intrebare = new Intrebare("Test?", "1)Da", "2)Nu", "3)Poate", "3", "Test");
            repository.addIntrebare(intrebare);
        } catch (DuplicateIntrebareException e) {
            assertFalse(e.getMessage().length() > 0);
        }
    }
}