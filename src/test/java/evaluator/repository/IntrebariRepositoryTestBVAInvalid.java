package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IntrebariRepositoryTestBVAInvalid {

    IntrebariRepository repository = new IntrebariRepository();


    @Test
    public void addIntrebareEnuntLung() {
        try {
            assertFalse(new Intrebare("TestasdfghjklqwertyuiopzxcvbnmasdfghjklqwertyuiopTestasdfghjklqwertyuiopzxcvbnmasdfghjklqwertyuiopadf?", "1)Da", "2)Nu", "3)Poate", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }
    @Test
    public void addIntrebareVarianta1Lung() {
        try {
            assertFalse(new Intrebare("Test2?", "1)Daasdfghjklqwertyuiopzxcvbnmqwertyuiopzxcvbnmasdf", "2)Nu", "3)Poate", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }
    @Test
    public void addIntrebareVarianta2Lung() {
        try {
            assertFalse(new Intrebare("Test3?", "1)Da", "2)Nuasdfghjklqwertyuiopzxcvbnmqwertyuiopzxcvbnmasdf", "3)Poate", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }
    @Test
    public void addIntrebareVarianta3Lung() {
        try {
            assertFalse(new Intrebare("Test4?", "1)Da", "2)Nu", "3)Poateasdfghjklqwertyuiopzxcvbnmqwertyuiopzxcvbnmf", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }
    @Test
    public void addIntrebareVariantaCorecta() {
        try {
            assertFalse(new Intrebare("Test5?", "1)Da", "2)Nu", "3)Poate", "0", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }
    @Test
    public void addIntrebareDomeniuLung() {
        try {
            assertFalse(new Intrebare("Test6?", "1)Da", "2)Nu", "3)Poate", "3", "Testwertyuaiopzxcvbnmqwertyuiop").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }
    @Test
    public void addIntrebareEnuntScurt() {
        try {
            assertFalse(new Intrebare("T", "1)Da", "2)Nu", "3)Poate", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }
    @Test
    public void addIntrebareVarianta1Scurt() {
        try {
            assertFalse(new Intrebare("Test1?", "1", "2)Nu", "3)Poate", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }
    @Test
    public void addIntrebareVarianta2Scurt() {
        try {
            assertFalse(new Intrebare("Test1?", "1)Da", "2", "3)Poate", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }
    @Test
    public void addIntrebareVarianta3Scurt(){
        try {
            assertFalse(new Intrebare("Test1?", "1)Da", "2)Nu", "3", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }
}