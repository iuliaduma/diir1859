package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IntrebariRepositoryTestECPInvalid {

    private IntrebariRepository repository = new IntrebariRepository();

    @Test
    public void addIntrebareVid() {
        try {
            assertFalse(new Intrebare("", "1)DA", "2)Nu", "3)Poate", "3", "Test").equals("Enuntul este vid!"));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length()>0);
        }
    }

    @Test
    public void addIntrebareDuplicate() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Test?", "1)DA", "2)Nu", "3)Poate", "3", "Test");
        /*intreabrea exista deja in lista*/
        repository.addIntrebare(intrebare);
        try {
            repository.addIntrebare(intrebare);
        } catch (DuplicateIntrebareException ex) {
            assertTrue(ex.getMessage().length() > 0);
        }
    }

    @Test
    public void addIntrebareLiteraMare() {
        /*enuntul intrebarii si deomeniul nu incep cu litera mare*/
        try {
            assertFalse(new Intrebare("test?", "1)DA", "2)Nu", "3)Poate", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length() > 0);
        }
    }

    @Test
    public void addIntrebareSfarsitEnunt() {
        /*enuntul intrebarii nu se termina cu ?*/
        try {
            assertFalse(new Intrebare("Test1", "1)DA", "2)Nu", "3)Poate", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length() > 0);
        }
    }

    @Test
    public void addIntrebareVariantadeRaspuns1() {
        /*variantele de raspuns nu incep cu '{nr varianta de raspuns(1, 2, 3)})'*/
        try {
            assertFalse(new Intrebare("Test?", "1DA", "2)Nu", "3)Poate", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length() > 0);
        }
    }

    @Test
    public void addIntrebareVariantadeRaspuns() {
        try {
            assertFalse(new Intrebare("Test?", ")DA", "2)Nu", "3)Poate", "3", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length() > 0);
        }
    }

    @Test
    public void addIntrebareVariantaCorecta(){

    /*varianta corecta nu apartine 1, 2 sau 3*/
        try {
            assertFalse(new Intrebare("Test?", "a)DA", "2)Nu", "3)Poate", "a", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length() > 0);
        }
    }
    @Test
    public void addIntrebareCorecta1(){
        try {
            assertFalse(new Intrebare("Test?", "1DA", "2)Nu", "3)Poate", "4", "Test").equals(""));
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().length() > 0);
        }
    }
}