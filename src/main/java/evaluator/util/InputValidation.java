package evaluator.util;


import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;

public class InputValidation {

	public static String validateEnunt(String enunt){
	
		enunt = enunt.trim();
		String msg="";
		if(enunt.equals(""))
			msg+="Enuntul este vid!\n";
		else {
			if (!Character.isUpperCase(enunt.charAt(0)))
				msg += "Prima litera din enunt nu e majuscula!\n";
			if (!String.valueOf(enunt.charAt(enunt.length() - 1)).equals("?"))
				msg += "Ultimul caracter din enunt nu e '?'!\n";
			if (enunt.length() > 100)
				msg += "Lungimea enuntului depaseste 100 de caractere!\n";
		}
		return msg;
	}
	
	public static String validateVarianta1(String varianta1){
		
		varianta1 = varianta1.trim();
		String msg="";
		if(varianta1.equals(""))
			msg+="Varianta1 este vida!\n";
		else {
			if(varianta1.length()<2)
				msg+="Varianta1 nu incepe cu '1)'!\n";
			else
			if (!String.valueOf(varianta1.charAt(0)).equals("1") || !String.valueOf(varianta1.charAt(1)).equals(")"))
					msg += "Varianta1 nu incepe cu '1)'!\n";
			if (varianta1.length() > 50)
				msg += "Lungimea variantei1 depaseste 50 de caractere!\n";
		}
		return msg;
	}
	
	public static String validateVarianta2(String varianta2){
		
		varianta2 = varianta2.trim();
		String msg="";
		if(varianta2.equals(""))
			msg+="Varianta2 este vida!\n";
		else {
			if(varianta2.length()<2)
				msg+="Varianta2 nu incepe cu '2)'!\n";
			else
				if (!String.valueOf(varianta2.charAt(0)).equals("2") || !String.valueOf(varianta2.charAt(1)).equals(")"))
					msg += "Varianta2 nu incepe cu '2)'!\n";
			if (varianta2.length() > 50)
				msg += "Lungimea variantei2 depaseste 50 de caractere!\n";
		}
		return msg;
	}
	
	public static String validateVarianta3(String varianta3){
		
		varianta3 = varianta3.trim();
		String msg="";
		if(varianta3.equals(""))
			msg+="Varianta3 este vida!\n";
		else {
			if(varianta3.length()<2)
				msg+="Varianta3 nu incepe cu '3)'!\n";
			else
				if (!String.valueOf(varianta3.charAt(0)).equals("3") || !String.valueOf(varianta3.charAt(1)).equals(")"))
					msg += "Varianta3 nu incepe cu '3)'!\n";
			if (varianta3.length() > 50)
				msg += "Lungimea variantei3 depaseste 50 de caractere!\n";
		}
		return msg;
	}
	
	public static String validateVariantaCorecta(String variantaCorecta){
		
		variantaCorecta = variantaCorecta.trim();
		String msg="";
		if(!variantaCorecta.equals("1") && !variantaCorecta.equals("2") && !variantaCorecta.equals("3"))
			msg="Varianta corecta nu este unul dintre caracterele {'1', '2', '3'}\n";
		return msg;
	}
	
	public static String validateDomeniu(String domeniu) {

		domeniu = domeniu.trim();
		String msg = "";
		if (domeniu.equals(""))
			msg += "Domeniul este vid!\n";
		else {
			if (!Character.isUpperCase(domeniu.charAt(0)))
				msg += "Prima litera din domeniu nu e majuscula!\n";
		if (domeniu.length() > 30)
			msg += "Lungimea domeniului depaseste 30 de caractere!\n";
		}
	return msg;
	}

	public static void validate(String enunt, String varianta1, String varianta2, String varianta3, String variantaCorecta, String domeniu) throws InputValidationFailedException {
		String msg="";
		msg+=validateEnunt(enunt);
		msg+=validateVarianta1(varianta1);
		msg+=validateVarianta2(varianta2);
		msg+=validateVarianta3(varianta3);
		msg+=validateVariantaCorecta(variantaCorecta);
		msg+=validateDomeniu(domeniu);
		if(!msg.equals(""))
			throw new InputValidationFailedException(msg);
	}
}
