package evaluator.main;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Test;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "intrebari.txt";
	
	public static void main(String[] args) throws IOException, InputValidationFailedException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		AppController appController = new AppController();
		
		boolean activ = true;
		String optiune = null;

		appController.loadIntrebariFromFile(file);
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			System.out.println("Introduceti optiune: ");
			optiune = console.readLine();

			switch(optiune){
			case "1" :
				System.out.println("Introduceti enuntul: ");
				String enunt= console.readLine();
				System.out.println("Introduceti varianta1: ");
				String varianta1= console.readLine();
				System.out.println("Introduceti varianta2: ");
				String varianta2= console.readLine();
				System.out.println("Introduceti varianta3: ");
				String varianta3= console.readLine();
				System.out.println("Introduceti variantaCorecta: ");
				String variantaCorecta= console.readLine();
				System.out.println("Introduceti domeniul: ");
				String domeniu= console.readLine();
				try {
					Intrebare intr= new Intrebare(enunt, varianta1,varianta2,varianta3,variantaCorecta, domeniu);
					appController.addNewIntrebare(intr);
					System.out.println(intr.toString());
				} catch (InputValidationFailedException e) {
					System.out.println(e.getMessage());
				} catch (DuplicateIntrebareException e) {
					System.out.println(e.getMessage());
				}
				break;
			case "2" :
				try {
					Test test = appController.createNewTest();
					System.out.println(test.toString());
				} catch (NotAbleToCreateTestException e) {
					System.out.println(e.getMessage());
				}
				break;
			case "3" :
			Statistica statistica;
			try {
				statistica = appController.getStatistica();
				System.out.println(statistica);
			} catch (NotAbleToCreateStatisticsException e) {
				// TODO
			}

			break;
			case "4" :
				appController.writeToFile(file);
				activ = false;
				break;
			default:
				break;
			}
		}
		
	}

}
